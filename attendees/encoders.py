from .models import Attendee
from common.json import ModelEncoder
from events.encoders import ConferenceListEncoder


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created", "conference"]

    encoders = {"conference": ConferenceListEncoder()}
